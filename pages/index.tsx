import type { NextPage } from "next";
import { motion, useScroll, useTransform } from "framer-motion";

import { useEffect, useState } from "react";

const Home: NextPage = () => {
  const { scrollY } = useScroll();
  const [showSecond, setShowSecond] = useState<Number>(0);
  const { scrollYProgress } = useScroll();
  const scale = useTransform(scrollYProgress, [0, 2], [1, -2]);
  const opacity = useTransform(scrollY, [200, 400], [1, 0]);

  const mainTextOpacity = useTransform(
    scrollY,
    [300, 700, 800, 1100],
    [0, 1, 1, 0]
  );

  const ipadVedio = useTransform(
    scrollY,
    [0, 900, 900, 2300, 2600, 12000000],
    ["", "", 0, 0, "", ""]
  );
  const ipadVedioPosition = useTransform(
    scrollY,
    [0, 800, 800, 2300, 2600, 12000000],
    ["", "", "fixed", "fixed", "hidden", "hidden"]
  );

  const ipadVedioOpacity = useTransform(
    scrollY,
    [0, 800, 800, 2300, 2600, 12000000],
    [0, 0, 1, 1, 0, 0]
  );

  const subTextOne = useTransform(
    scrollY,
    [950, 1100, 1200, 1400],
    [0, 1, 1, 0]
  );

  const subTextTwo = useTransform(
    scrollY,
    [1250, 1400, 1500, 1700],
    [0, 1, 1, 0]
  );

  const subTextThree = useTransform(
    scrollY,
    [1550, 1700, 1800, 2000],
    [0, 1, 1, 0]
  );

  const subTextFour = useTransform(
    scrollY,
    [1850, 2000, 2100, 2300],
    [0, 1, 1, 0]
  );

  const watchEvent = useTransform(
    scrollY,
    [2150, 2200, 2300, 2300],
    [0, 1, 1, 1]
  );

  const mainSubOpacity = useTransform(scrollY, [2300, 2400], [0, 1]);

  const backgroundColor = useTransform(
    scrollY,
    [0, 2460, 2460, 2480, 2490, 200000],
    ["#000000", "#000000", "#D3D3D3", "#D3D3D3", "#ffffff", "#ffffff"]
  );

  const text = useTransform(
    scrollY,
    [0, 2460, 2460, 2480, 2490, 200000],
    ["#ffffff", "#ffffff", "#808080", "#808080", "#000000", "#ffffff"]
  );

  useEffect(() => {
    return scrollY.onChange((latest) => {
      setShowSecond(latest);
      console.log("Page scroll: ", latest);
    });
  });

  return (
    <motion.div
      animate={{}}
      transition={{ duration: 2,
      ease:"easeInOut" }}
      style={{
        backgroundColor: backgroundColor,
      }}
      className=" space-y-6  bg-black "
    >
      <div className="h-[100vh] -z-20 ">
        <motion.div
          className={`flex  transition-all duration-400 ease-in-out flex-col h-[100vh]  fixed top-0 items-center justify-center   bg-black  
            
          `}
          style={{
            scale,
            opacity: opacity,
          }}
        >
          <video
            muted
            autoPlay
            width="60%"
            height="60%"
            src="https://www.apple.com/105/media/us/ipad-air/2022/5abf2ff6-ee5b-4a99-849c-a127722124cc/anim/hero/large_2x.mp4"
          ></video>
          <motion.img
            src="https://www.apple.com/v/ipad-air/r/images/overview/hero/main_ipad__d991v5y9hgom_large.png"
            className=" absolute w-2/3 h-2/3 bg-transparent  "
            alt=""
          />
        </motion.div>
      </div>

      <motion.div
        // initial={{ y: 200, opacity: 0 }}
        className=" w-full pb-6 mb-[70px] flex items-center justify-center h-[100vh] bg-black"
        // animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 2 }}
        style={{
          opacity: mainTextOpacity,
        }}
      >
        <motion.h1 className="text-9xl font-medium text-center align-middle text-transparent bg-clip-text   bg-gradient-to-r text-clip from-[#dc79ff] to-[#256bfa] z-50">
          Light. Bright. <br /> Full of might.
        </motion.h1>
      </motion.div>

      <motion.section className="pb-20">
        <motion.div
          className={`z-10`}
          style={{
            top: ipadVedio,
            position: ipadVedioPosition,
            opacity: ipadVedioOpacity,
          }}
          transition={{ duration: 3 }}
        >
          <video
            src="air.mp4"
            className="opacity-30  object-cover"
            muted
            autoPlay
            loop
            id="myVideo"
          ></video>
        </motion.div>

        <div className="space-y-10 z-50">
          <motion.div
            className="text-white w-full pb-6 flex items-center justify-center"
            style={{
              opacity: subTextOne,
            }}
            transition={{ duration: 2 }}
          >
            <motion.h1 className="text-[72px] font-medium text-center align-middle   z-50 ">
              Supercharged by the <br /> Apple M1 chip.
            </motion.h1>
          </motion.div>

          <motion.div
            className="text-white w-full pb-6 flex items-center justify-center"
            style={{
              opacity: subTextTwo,
            }}
            transition={{ duration: 2 }}
          >
            <motion.h1 className="text-[72px] font-medium text-center align-middle   z-50  ">
              12MP Ultra Wide front camera <br /> with Center Stage.
            </motion.h1>
          </motion.div>

          <motion.div
            className="text-white w-full pb-6 flex items-center justify-center"
            style={{
              opacity: subTextThree,
            }}
            transition={{ duration: 2 }}
          >
            <motion.h1 className="text-[72px] font-medium text-center align-middle     z-50 ">
              Blazing-fast 5G.
            </motion.h1>
          </motion.div>

          <motion.div
            className="text-white w-full pb-6 flex items-center justify-center"
            style={{
              opacity: subTextFour,
            }}
            transition={{ duration: 2 }}
          >
            <motion.h1 className="text-[72px] font-medium text-center align-middle     z-50 ">
              Five gorgeous colors.
            </motion.h1>
          </motion.div>
        </div>
      </motion.section>
      <div className="flex gap-7 justify-center text-2xl text-[#2997ff] font-normal z-50">
        <p>Watch the film</p>
        <p>Watch the event</p>
      </div>
      <motion.div
        // initial={{ y: 200, opacity: 0 }}
        className=" w-full pb-6 mb-[70px] flex items-center justify-start h-[100vh] pl-40"
        // animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 2 }}
        style={{
          opacity: mainSubOpacity,
        }}
      >
        <motion.h1
          style={{
            color: text,
          }}
          className="text-[64px] font-medium text-left align-middle  z-50"
        >
          <span>All-screen</span>{" "}
          <span className="text-transparent bg-clip-text   bg-gradient-to-r text-clip from-[#f56772] to-[#ba6bff]">
            design.
          </span>{" "}
          <br /> <span>Beauty all around.</span>
        </motion.h1>
      </motion.div>
    </motion.div>
  );
};

export default Home;
